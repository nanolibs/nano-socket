# nanolibs - Socket
Provides a C++17, **POSIX-only** interface for sockets (nothing more!). No fancy stuff just the bare api!

## What the heck are nanolibs?
A dependency on a huge library just to have a small subset of its features is a very bad situation and a programmer should not be forced to do that.

The nanolibs libraries provide a very small layer of abstraction to decrease the foot print of your executable and maintenance cost of your project.

## Why a socket library?
Socket programming can be a very challenging task and often developers use other libraries to avoid calling the os api directly. But most of the libraries introduce a large dependency and a burden to maintain.

Also library specific characteristics take place which affect the rest of your application. This can cause problems with other libraries or forces a specific philosophy onto your software and makes replacing the library later more difficult.

## So what is the benefit?
Using the socket nano-library brings a smallest possible abstraction layer of the POSIX socket api to provide a secure and comofortable c++17 interface. It reliefs you from struggling with C interfaces.

It uses exceptions and descriptive, easy-to-use interfaces to make your life easier.  

## How do I use it?

*Check out the 'examples' folder!*