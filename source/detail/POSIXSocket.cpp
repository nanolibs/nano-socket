#include "nanolibs/socket/types.h"
#include "nanolibs/socket/detail/POSIXSocket.h"
#include "nanolibs/socket/exceptions.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>


//--- Configuration
namespace {

} // namespace

//--- Hepler
namespace {

using namespace nanolibs::socket;

template <class T, class TAl>
inline T* begin_ptr(std::vector<T,TAl>& v)
{return  v.empty() ? 0 : &v[0];}

template <class T, class TAl>
inline const T* begin_ptr(const std::vector<T,TAl>& v)
{return  v.empty() ? 0 : &v[0];}

inline void CreateAddress(sockaddr_in& target, const std::string_view& address, const AddressFamily addressFamily,
                          const int port)
{
  ::memset(&target, 0, sizeof(target));
  ::inet_aton(std::string(address).data(), &target.sin_addr);
  target.sin_family = static_cast<sa_family_t >(addressFamily);
  target.sin_port = ::htons(static_cast<uint16_t>(port));
}

} // namespace

//--- API
namespace nanolibs::socket::detail {

FileDescriptor POSIXSocket::create(const AddressFamily addressFamily, const SocketType type,
                                   const NetworkProtocol networkProtocol, const SocketBehavior behavior) {
  auto error = ::socket(static_cast<int>(addressFamily), static_cast<int>(type) | static_cast<int>(behavior),
                        static_cast<int>(networkProtocol));

  if (error >= 0) {
    return static_cast<FileDescriptor>(error);
  }

  if (errno == EACCES) {
    throw exception::PermissionDenied(
        "Permission to create a socket of the specified type and/or protocol is denied.");
  } else if (errno == EAFNOSUPPORT) {
    throw exception::AddressFamilyNotSupported("The implementation does not support the specified address family.");
  } else if (errno == EINVAL) {
    throw exception::WrongProtocolOrSocketType(
        "Unknown protocol, or protocol family not available or invalid flags in socket type.");
  } else if (errno == EMFILE) {
    throw exception::MaximumOpenFileDescriptorsExceeded(
        "The per-process or system-wide limit on the number of open file descriptors has been reached.");
  } else if (errno == ENOBUFS || errno == ENOMEM) {
    throw exception::InsufficientMemory(
        "Insufficient memory is available. The socket cannot be created until sufficient resources are freed.");
  } else if (errno == EPROTONOSUPPORT) {
    throw exception::ProtocolTypeNotSupported(
        "The protocol type or the specified protocol is not supported within this domain.");
  }

  throw exception::UnknownError("Call to ::socket returned unknown error code " + std::to_string(error) + "!");
}

void POSIXSocket::bind(const FileDescriptor fd, const std::string_view& address, const AddressFamily addressFamily, const int port) {
  sockaddr_in serv_addr;

  CreateAddress(serv_addr, address, addressFamily, port);

  auto error = ::bind(fd, reinterpret_cast<sockaddr*>(&serv_addr), sizeof(serv_addr));

  if (error == 0) {
    return;
  }

  if (errno == EACCES) {
    throw exception::PermissionDenied(
        "The address is protected, and the user is not the superuser. For UNIX domain sockets also: Search "
        "permission is denied on a component of the path prefix.");
  } else if (errno == EADDRINUSE) {
    throw exception::AddressInUse(
        "The given address is already in use. Or for Internet domain sockets: The port number was specified as zero "
        "in the socket address structure, but, upon attempting to bind to an ephemeral port, it was determined that "
        "all port numbers in the ephemeral port range are currently in use. See the discussion of "
        "/proc/sys/net/ipv4/ip_local_port_range.");
  } else if (errno == EBADF) {
    throw exception::BadFileDescriptor("sockfd is not a valid file descriptor.");
  } else if (errno == EINVAL) {
    throw exception::SocketAlreadyBoundOrInvalidAddress(
        "The socket is already bound to an address or addrlen is wrong, or addr is not a valid address for this "
        "socket's domain.");
  } else if (errno == ENOTSOCK) {
    throw exception::FileDescriptorNotASocket("The file descriptor sockfd does not refer to a socket.");
  } else if (errno == EADDRNOTAVAIL) {
    throw exception::NonLocalAddressOrInterfaceNotExisting(
        "UNIX domain sockets: A nonexistent interface was requested or the requested address was not local.");
  } else if (errno == EFAULT) {
    throw exception::ShouldNotHappenException("::bind returned error code " + std::to_string(EFAULT) + "(EFAULT)");
  } else if (errno == ELOOP) {
    throw exception::TooManySymbolicLinks("UNIX domain sockets: Too many symbolic links were encountered in resolving addr.");
  } else if (errno == ENAMETOOLONG) {
    throw exception::AddressToLong("UNIX domain sockets: addr is too long.");
  } else if (errno == ENOENT) {
    throw exception::MissingComponent(
        "UNIX domain sockets: A component in the directory prefix of the socket pathname does not exist.");
  } else if (errno == ENOMEM) {
    throw exception::InsufficientMemory("UNIX domain sockets: Insufficient kernel memory was available.");
  } else if (errno == ENOTDIR) {
    throw exception::ComponentIsNotADirectory("UNIX domain sockets: A component of the path prefix is not a directory.");
  } else if (errno == EROFS) {
    throw exception::ReadOnlyFilesystem("UNIX domain sockets: The socket inode would reside on a read-only filesystem.");
  }

  throw exception::UnknownError("Call to ::bind returned unknown error code " + std::to_string(error) + "!");
}

void POSIXSocket::connect(const FileDescriptor& fd, const std::string_view& address, const AddressFamily addressFamily,
                          const int port) {
  static sockaddr_in serv_addr;

  CreateAddress(serv_addr, address, addressFamily, port);

  auto error = ::connect(fd, reinterpret_cast<sockaddr*>(&serv_addr), sizeof(serv_addr));

  if (error == 0) {
    return;
  }

  if (errno == EACCES) {
    throw exception::PermissionDenied(
        "For UNIX domain sockets, which are identified by pathname: Write permission is denied on the socket file, or "
        "search permission is denied for one of the directories in the path prefix.\n"
        "Or:\n"
        "The user tried to connect to a broadcast address without having the socket broadcast flag enabled or the "
        "connection request failed because of a local firewall rule.");
  } else if (errno == EPERM) {
    throw exception::PermissionDenied(
        "The user tried to connect to a broadcast address without having the socket broadcast flag enabled or the "
        "connection request failed because of a local firewall rule.");
  } else if (errno == EADDRINUSE) {
    throw exception::AddressInUse("Local address is already in use.");
  } else if (errno == EADDRNOTAVAIL) {
    throw exception::NonLocalAddressOrInterfaceNotExisting(
        "(Internet domain sockets) The socket referred to by sockfd had not previously been bound to an address and, "
        " upon attempting to bind it to an ephemeral port, it was determined that all port numbers in the ephemeral "
        "port range are currently in use.");
  } else if (errno == EAFNOSUPPORT) {
    throw exception::AddressFamilyNotSupported(
        "The passed address didn't have the correct address family in its sa_family field.");
  } else if (errno == EAGAIN) {
    throw exception::Again(
        "For nonblocking UNIX domain sockets, the socket is nonblocking, and the connection cannot be completed "
        "immediately.  For other socket families, there are insufficient entries in the routing cache.");
  } else if (errno == EALREADY) {
    throw exception::SocketAlreadyInUse(
        "The socket is nonblocking and a previous connection attempt has not yet been completed.");
  } else if (errno == EBADF) {
    throw exception::BadFileDescriptor("sockfd is not a valid open file descriptor.");
  } else if (errno == ECONNREFUSED) {
    throw exception::ConnectionRefused(
        "A connect() on a stream socket found no one listening on the remote address.");
  } else if (errno == EFAULT) {
    throw exception::ShouldNotHappenException("The socket structure address is outside the user's address space.");
  } else if (errno == EINPROGRESS) {
    throw exception::ConnectionInProgress(
        "The socket is nonblocking and the connection cannot be completed immediately.  (UNIX domain sockets failed "
        "with EAGAIN instead.)  It is possible to select(2) or poll(2) for completion by selecting the socket for "
        "writing. After select(2) indicates writability, use getsockopt(2) to read the SO_ERROR option at level "
        "SOL_SOCKET to determine whether connect() completed successfully (SO_ERROR is zero) or unsuccessfully "
        "(SO_ERROR is one of the usual error codes listed here, explaining the reason for the failure).");
  } else if (errno == EINTR) {
    throw exception::Interrupted("The system call was interrupted by a signal that was caught.");
  } else if (errno == EISCONN) {
    throw exception::SocketAlreadyConnected("The socket is already connected.");
  } else if (errno == ENETUNREACH) {
    throw exception::NetworkUnreachable("Network is unreachable.");
  } else if (errno == ENOTSOCK) {
    throw exception::FileDescriptorNotASocket("The file descriptor sockfd does not refer to a socket.");
  } else if (errno == EPROTOTYPE) {
    throw exception::ProtocolTypeNotSupported(
        "The socket type does not support the requested communications protocol. This error can occur, for example, "
        "on an attempt to connect a UNIX domain datagram socket to a stream socket.");
  } else if (errno == ETIMEDOUT) {
    throw exception::TimedOut("Timeout while attempting connection.  The server may be too busy to accept new "
                              "connections. Note that for IP sockets the timeout may be very long when syncookies are "
                              "enabled on the server.");
  }
}

void POSIXSocket::listen(const FileDescriptor& fd, int backlog) {
  auto error = ::listen(fd, backlog);

  if (error == 0) {
    return;
  }

  if (errno == EADDRINUSE) {
    throw exception::AddressInUse("Another socket is already listening on the same port. Or (Internet domain sockets) "
                                  "The socket referred to by sockfd had not previously been bound to an address and, "
                                  "upon attempting to bind it to an ephemeral port, it was determined that all port "
                                  "numbers in the ephemeral port range are currently in use. See the discussion of "
                                  "/proc/sys/net/ipv4/ip_local_port_range in ip(7).");
  } else if (errno == EBADF) {
    throw exception::BadFileDescriptor("The argument sockfd is not a valid file descriptor.");
  } else if (errno == ENOTSOCK) {
    throw exception::FileDescriptorNotASocket("The file descriptor sockfd does not refer to a socket.");
  } else if (errno == EOPNOTSUPP) {
    throw exception::NotSupported("The socket is not of a type that supports the listen() operation.");
  }

  throw exception::UnknownError("listen");
}

    namespace {

        void HandleAcceptErrors(const int error, const int flags) {
            if (error == EAGAIN || error == EWOULDBLOCK) {
                throw exception::Again("The socket is marked nonblocking and no connections are present to be accepted. "
                                       "POSIX.1-2001 and POSIX.1-2008 allow either error to be returned for this case, and do not "
                                       "require these constants to have the same value, so a portable application should check for "
                                       "both possibilities.");
            } else if (error == EBADF) {
                throw exception::BadFileDescriptor("Bad file descriptor");
            } else if (error == EFAULT) {
                throw exception::ShouldNotHappenException("The addr argument is not in a writable part of the user address space.");
            } else if (error == EINTR) {
                throw exception::Interrupted("The system call was interrupted by a signal that was caught before a valid "
                                             "connection arrived.");
            } else if (error == EINVAL) {
                throw exception::SocketIsNotListening("Socket is not listening for connections.");
            } else if (error == EMFILE) {
                throw exception::MaximumOpenFileDescriptorsExceeded("The per-process limit on the number of open file descriptors "
                                                                    "has been reached.");
            } else if (error == ENFILE) {
                throw exception::MaximumOpenFileDescriptorsExceeded("The system-wide limit on the total number of open files has "
                                                                    "been reached.");
            } else if (error == ENOBUFS || error == ENOMEM) {
                throw exception::InsufficientMemory("Not enough free memory. This often means that the memory allocation is "
                                                    "limited by the socket buffer limits, not by the system memory.");
            }

            throw exception::UnknownError("accept");
        }

    }

FileDescriptor POSIXSocket::accept(const FileDescriptor &fd) {
  auto error = ::accept(fd, NULL, NULL);

  if (error >= 0) {
    return static_cast<FileDescriptor>(error);
  }

  HandleAcceptErrors(errno, 0);
  return -1;
}

FileDescriptor POSIXSocket::accept(const FileDescriptor &fd,
                                   const std::string_view &address,
                                   AddressFamily addressFamily,
                                   int port,
                                   AcceptFlags flags) {
  static sockaddr_in serv_addr;
  CreateAddress(serv_addr, address, addressFamily, port);
  socklen_t socketlen = sizeof(serv_addr);
  auto error = ::accept(fd, reinterpret_cast<sockaddr*>(&serv_addr), &socketlen);

  if (error >= 0) {
    return static_cast<FileDescriptor>(error);
  }

  HandleAcceptErrors(errno, static_cast<int>(flags));
  return -1;
}

void POSIXSocket::write(const FileDescriptor &fd, const std::vector<uint8_t>& data) {
  auto error = ::write(static_cast<int>(fd), static_cast<const void*>(begin_ptr(data)), data.size());

  if (error >= 0) {
    return;
  }

  if (errno == EAGAIN || errno == EWOULDBLOCK) {
    throw exception::WouldBlock("The file descriptor fd refers to a file other than a socket and has been marked "
                                "non-blocking (O_NONBLOCK), and the write would block.");
  } else if (errno == EBADFD) {
    throw exception::BadFileDescriptor("fd is not a valid file descriptor or is not open for writing.");
  } else if (errno == EDESTADDRREQ) {
    throw exception::DestinationAddressRequired("fd refers to a datagram socket for which a peer address has not been "
                                                "set using connect(2).");
  } else if (errno == EDQUOT) {
    throw exception::DiskQuotaExhausted("The user's quota of disk blocks on the file system containing the file "
                                        "referred to by fd has been exhausted.");
  } else if (errno == EFAULT) {
    throw exception::ShouldNotHappenException("buf is outside your accessible address space.");
  } else if (errno == EFBIG) {
    throw exception::FileTooBig("An attempt was made to write a file that exceeds the implementation-defined maximum "
                                "file size or the process's file size limit, or to write at a position past the "
                                "maximum allowed offset.");
  } else if (errno == EINTR) {
    throw exception::Interrupted("The call was interrupted by a signal before any data was written.");
  } else if (errno == EINVAL) {
    throw exception::TargetUnsuitableForWriting(" is attached to an object which is unsuitable for writing; or the "
                                                "file was opened with the O_DIRECT flag, and either the address "
                                                "specified in buf, the value specified in count, or the current "
                                                "file offset is not suitably aligned.");
  } else if (errno == EIO) {
    throw exception::LowLevelIOError("A low-level I/O error occurred while modifying the inode.");
  } else if (errno == ENOSPC) {
    throw exception::NotEnoughSpace("The device containing the file referred to by fd has no room for the data.");
  } else if (errno == EPIPE) {
    throw exception::PipeClosed("fd is connected to a pipe or socket whose reading end is closed. When this happens "
                                "the writing process will also receive a SIGPIPE signal. (Thus, the write return value "
                                "is seen only if the program catches, blocks or ignores this signal.)");
  } else {
    throw exception::UnknownError("write");
  }
}

size_t POSIXSocket::read(const FileDescriptor &fd, std::vector<uint8_t>& data, const size_t count) {
  data.resize(count);
  auto error = ::read(static_cast<int>(fd), static_cast<void*>(begin_ptr(data)), count);

  if (error >= 0) {
    return error;
  }

  if (errno == EAGAIN || errno == EWOULDBLOCK) {
    throw exception::WouldBlock("The file descriptor fd refers to a file other than a socket and has been marked "
                                "non-blocking (O_NONBLOCK), and the read would block.");
  } else if (errno == EBADFD) {
    throw exception::BadFileDescriptor("fd is not a valid file descriptor or is not open for reading.");
  } else if (errno == EFAULT) {
    throw exception::ShouldNotHappenException("buf is outside your accessible address space.");
  } else if (errno == EINTR) {
    throw exception::Interrupted("The call was interrupted by a signal before any data was written.");
  } else if (errno == EINVAL) {
    throw exception::TargetUnsuitableForWriting("fd is attached to an object which is unsuitable for reading; or the "
                                                "file was opened with the O_DIRECT flag, and either the address "
                                                "specified in buf, the value specified in count, or the current "
                                                "file offset is not suitably aligned.\n - or -\n"
                                                "fd was created via a call to timerfd_create(2) and the wrong size "
                                                "buffer was given to read(); see timerfd_create(2) for further "
                                                "information.");
  } else if (errno == EIO) {
    throw exception::LowLevelIOError("I/O error. This will happen for example when the process is in a background "
                                     "process group, tries to read from its controlling terminal, and either it is "
                                     "ignoring or blocking SIGTTIN or its process group is orphaned. It may also occur "
                                     "when there is a low-level I/O error while reading from a disk or tape.");
  } else if (errno == EISDIR) {
    throw exception::IsDirectory("fd refers to a directory.");
  } else {
    throw exception::UnknownError("read");
  }
}

void POSIXSocket::close(const FileDescriptor& fd) {
  auto error = ::close(fd);

  if (error == 0) {
    return;
  }

  if (errno == EBADF) {
    throw exception::BadFileDescriptor("The argument is not a valid file descriptor.");
  } else if (errno == EINTR) {
    throw exception::Interrupted("The close() function was interrupted by a signal.");
  } else if (errno == EIO) {
    throw exception::LowLevelIOError("An I/O error occurred while reading from or writing to the file system.");
  }

  throw exception::UnknownError("close");
}

namespace {

void HandleReceiveErrors(const int error) {
  if (error == EAGAIN || error == EWOULDBLOCK) {
    throw exception::WouldBlock("The socket is marked nonblocking and the receive operation would block, or a receive "
                                "timeout had been set and the timeout expired before data was received.  POSIX.1 "
                                "allows either error to be returned for this case, and does not require these "
                                "constants to have the same value, so a portable application should check for both "
                                "possibilities.");
  } else if (error == EBADF) {
    throw exception::BadFileDescriptor("The argument sockfd is an invalid file descriptor.");
  } else if (error == ECONNREFUSED) {
    throw exception::ConnectionRefused("A remote host refused to allow the network connection (typically because it "
                                       "is not running the requested service).");
  } else if (error == EFAULT) {
    throw exception::ShouldNotHappenException("The receive buffer pointer(s) point outside the process's address "
                                              "space.");
  } else if (error == EINTR) {
    throw exception::Interrupted("The receive was interrupted by delivery of a signal before any data was available; "
                                 "see signal(7).");
  } else if (error == EINVAL) {
    throw exception::ShouldNotHappenException("Invalid argument passed.");
  } else if (error == ENOTCONN) {
    throw exception::NotConnected("The socket is associated with a connection-oriented protocol and has not been "
                                  "connected (see connect(2) and accept(2)).");
  } else if (error == ENOTSOCK) {
    throw exception::FileDescriptorNotASocket("The file descriptor sockfd does not refer to a socket.");
  }

  throw exception::UnknownError("recv");
}

}

ssize_t POSIXSocket::recv(const FileDescriptor& fd, std::vector<uint8_t> &data, size_t count, ReceiveFlags flags) {
  data.resize(count);
  auto error = ::recv(static_cast<int>(fd), static_cast<void*>(begin_ptr(data)), count, static_cast<int>(flags));

  if (error >= 0) {
    return static_cast<ssize_t>(error);
  }

  HandleReceiveErrors(error);
  return 0;
}

void POSIXSocket::shutdown(const FileDescriptor& fd) {
  auto error = ::shutdown(fd, SHUT_RDWR);

  if (error == 0) {
    return;
  }

  throw exception::UnknownError(std::strerror(errno));
}

} // namespace nanolibs::socket::detail::socket
