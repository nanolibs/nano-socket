#include <nanolibs/socket/exception/Again.h>
#include <nanolibs/socket/exception/ShouldNotHappenException.h>
#include "nanolibs/Socket.h"
#include "nanolibs/socket/exceptions.h"
#include "nanolibs/socket/detail/POSIXSocket.h"

namespace nanolibs {

Socket::Socket(const socket::FileDescriptor& fd) noexcept
  : m_fd(fd)
  , m_api(std::make_shared<socket::detail::POSIXSocket>()) { }

Socket::Socket(const std::shared_ptr<socket::Api>& api, const socket::FileDescriptor& fd) noexcept
    : m_fd(fd)
    , m_api(api) { }

Socket::Socket(socket::AddressFamily addressFamily,
               socket::SocketType type,
               socket::NetworkProtocol networkProtocol,
               socket::SocketBehavior behavior) noexcept
    : m_api(std::make_shared<socket::detail::POSIXSocket>()) {
  m_fd = m_api->create(addressFamily, type, networkProtocol, behavior);
}

Socket::Socket(const std::shared_ptr<socket::Api>& api,
               socket::AddressFamily addressFamily,
               socket::SocketType type,
               socket::NetworkProtocol networkProtocol,
               socket::SocketBehavior behavior) noexcept
    : m_api(api) {
  m_fd = m_api->create(addressFamily, type, networkProtocol, behavior);
}

Socket::~Socket() { close(); }

void Socket::listen(int backlog) { m_api->listen(m_fd, backlog); }

std::unique_ptr<Socket> Socket::accept() { return std::make_unique<Socket>(m_api->accept(m_fd)); }

std::unique_ptr<Socket> Socket::accept(const std::string_view &address,
                              socket::AddressFamily addressFamily,
                              int port,
                              socket::AcceptFlags flags) {
  return std::make_unique<Socket>(m_api->accept(m_fd, address, addressFamily, port, flags));
}

void Socket::bind(const std::string_view &address, const socket::AddressFamily addressFamily, const int port) {
  m_api->bind(m_fd, address, addressFamily, port);
}

void Socket::connect(const std::string_view &address, socket::AddressFamily addressFamily, int port) {
  m_api->connect(m_fd, address, addressFamily, port);
}

void Socket::close() {
  try {
    m_api->close(m_fd);
  } catch (const socket::exception::Again&) {
    try {
      m_api->close(m_fd);
    } catch (const socket::exception::Again&) {
      throw socket::exception::ShouldNotHappenException("socket::exception::Again thrown too often.");
    } catch (...) {
      throw;
    }
  } catch (...) {
    throw;
  }
}

void Socket::write(const std::vector<uint8_t> &data) {
  m_api->write(m_fd, data);
}

size_t Socket::read(std::vector<uint8_t> &data, size_t count) {
  return m_api->read(m_fd, data, count);
}

socket::FileDescriptor Socket::fileDescriptor() const noexcept {
  return m_fd;
}

ssize_t Socket::recv(std::vector<uint8_t> &data, size_t count, socket::ReceiveFlags flags) {
  return m_api->recv(m_fd, data, count, flags);
}


} // namespace nanolibs
