#include "nanolibs/Socket.h"

#include <catch2/catch.hpp>
#include <memory>

using namespace nanolibs;


namespace {

class SocketApiMock : public socket::Api {


public:
  int m_createCalled = 0;
  int m_bindCalled = 0;
  int m_connectCalled = 0;
  int m_writeCalled = 0;
  int m_readCalled = 0;
  int m_closeCalled = 0;

  SocketApiMock() = default;
  ~SocketApiMock() = default;

  socket::FileDescriptor create(socket::AddressFamily addressFamily, socket::SocketType type, socket::NetworkProtocol networkProtocol, socket::SocketBehavior behavior) override {
    ++m_createCalled;
    return 0;
  }

  void bind(const socket::FileDescriptor fd,
            const std::string_view &address,
            const socket::AddressFamily addressFamily,
            const int port) override {
    ++m_bindCalled;
  }

  void connect(const socket::FileDescriptor &fd,
               const std::string_view &address,
               socket::AddressFamily addressFamily,
               int port) override {
    ++m_connectCalled;
  }

  void listen(const socket::FileDescriptor&, int) override { }

  socket::FileDescriptor accept(const socket::FileDescriptor &fd) override { return 1; }

  socket::FileDescriptor accept(const socket::FileDescriptor &fd,
                                const std::string_view &address,
                                socket::AddressFamily addressFamily,
                                int port,
                                socket::AcceptFlags flags) override {
    return 1;
  }

  void close(const socket::FileDescriptor &fd) override {
    ++m_closeCalled;
  }

  void write(const socket::FileDescriptor &fd, const std::vector<uint8_t> &data) override {
    ++m_writeCalled;
  }

  size_t read(const socket::FileDescriptor &fd, std::vector<uint8_t> &data, size_t count) override {
    ++m_readCalled;
    return 0;
  }


  ssize_t recv(const socket::FileDescriptor& fd, std::vector<uint8_t> &data, size_t count, socket::ReceiveFlags flags) override {
    return 0;
  }

  void shutdown(const socket::FileDescriptor& fd) override {

  }
};

}

TEST_CASE( "POSIXSocket proxy", "[single-file]" ) {
  auto mock = std::make_shared<SocketApiMock>();
  {
    Socket socket(mock,
                  socket::AddressFamily::Inet,
                  socket::SocketType::Stream,
                  socket::NetworkProtocol::Tcp,
                  socket::SocketBehavior::Block);

    REQUIRE(mock->m_createCalled == 1);
    REQUIRE(mock->m_bindCalled == 0);
    REQUIRE(mock->m_connectCalled == 0);
    REQUIRE(mock->m_writeCalled == 0);
    REQUIRE(mock->m_readCalled == 0);
    REQUIRE(mock->m_closeCalled == 0);

    socket.connect("", socket::AddressFamily::Inet, 0);

    REQUIRE(mock->m_createCalled == 1);
    REQUIRE(mock->m_bindCalled == 0);
    REQUIRE(mock->m_connectCalled == 1);
    REQUIRE(mock->m_writeCalled == 0);
    REQUIRE(mock->m_readCalled == 0);
    REQUIRE(mock->m_closeCalled == 0);

    socket.bind("", socket::AddressFamily::Inet, 0);

    REQUIRE(mock->m_createCalled == 1);
    REQUIRE(mock->m_bindCalled == 1);
    REQUIRE(mock->m_connectCalled == 1);
    REQUIRE(mock->m_writeCalled == 0);
    REQUIRE(mock->m_readCalled == 0);
    REQUIRE(mock->m_closeCalled == 0);

    std::vector<uint8_t> d;
    socket.read(d, 0);

    REQUIRE(mock->m_createCalled == 1);
    REQUIRE(mock->m_bindCalled == 1);
    REQUIRE(mock->m_connectCalled == 1);
    REQUIRE(mock->m_writeCalled == 0);
    REQUIRE(mock->m_readCalled == 1);
    REQUIRE(mock->m_closeCalled == 0);

    socket.write(d);

    REQUIRE(mock->m_createCalled == 1);
    REQUIRE(mock->m_bindCalled == 1);
    REQUIRE(mock->m_connectCalled == 1);
    REQUIRE(mock->m_writeCalled == 1);
    REQUIRE(mock->m_readCalled == 1);
    REQUIRE(mock->m_closeCalled == 0);
  }

  REQUIRE(mock->m_createCalled == 1);
  REQUIRE(mock->m_bindCalled == 1);
  REQUIRE(mock->m_connectCalled == 1);
  REQUIRE(mock->m_writeCalled == 1);
  REQUIRE(mock->m_readCalled == 1);
  REQUIRE(mock->m_closeCalled == 1);
}

