#pragma once

#include "nanolibs/Socket.h"

namespace common {

class StreamHelper final {
private:
  StreamHelper() = delete;
  ~StreamHelper() = default;

public:
  static void sendString(nanolibs::Socket& socket, const std::string& message) {
    std::vector<uint8_t> buffer{static_cast<uint8_t>(message.length() >> 24),
                                static_cast<uint8_t>(message.length() >> 16),
                                static_cast<uint8_t>(message.length() >> 8),
                                static_cast<uint8_t>(message.length())};
    std::copy(message.begin(), message.end(), std::back_inserter(buffer));
    socket.write(buffer);
  }

  static std::string readString(nanolibs::Socket& socket) {
    std::vector<uint8_t> buffer;
    auto len = socket.read(buffer, 4);

    if (len == 4) {
      size_t strlen = (buffer[0] << 24)
          + (buffer[1] << 16)
          + (buffer[2] << 8)
          + buffer[3];
      buffer.clear();

      socket.read(buffer, strlen);
      std::string result;
      std::copy(buffer.begin(), buffer.end(), std::back_inserter(result));
      return result;
    }

    return "";
  }
};

}
