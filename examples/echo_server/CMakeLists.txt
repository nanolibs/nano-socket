add_executable(echo_server main.cpp)
target_include_directories(echo_server PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/../common")
target_link_libraries(echo_server Socket pthread)