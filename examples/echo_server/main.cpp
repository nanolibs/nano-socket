#include <chrono>
#include <iostream>
#include <thread>

#include "StreamHelper.h"

#include "nanolibs/Socket.h"

using namespace std::chrono_literals;


int server_main(int argc, char** argv) {
  nanolibs::Socket server(nanolibs::socket::AddressFamily::Inet,
                          nanolibs::socket::SocketType::Stream,
                          nanolibs::socket::NetworkProtocol::Tcp,
                          nanolibs::socket::SocketBehavior::Block);

  server.bind("0.0.0.0", nanolibs::socket::AddressFamily::Inet, 9191);
  server.listen(2);

  auto newClient = server.accept();
  if (newClient) {
    std::string message = common::StreamHelper::readString(*newClient.get());
    common::StreamHelper::sendString(*newClient.get(), message);
    std::cout << "Received: " << message << std::endl;
  }

  return EXIT_SUCCESS;
}

int main(int argc, char** argv) {
  try {
    return server_main(argc, argv);
  } catch (const nanolibs::socket::exception::Exception& ex) {
    std::cout << "----- Socket Exception -----" << std::endl;
    std::cout << "What: " << ex.what() << std::endl;
    std::cout << "Details: " << ex.details() << std::endl;
    std::cout << std::string(10, '=') << std::endl;
  }
}
