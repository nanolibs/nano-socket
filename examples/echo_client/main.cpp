#include <chrono>
#include <cstdint>
#include <iostream>
#include <thread>
#include <vector>

#include "StreamHelper.h"

#include "nanolibs/Socket.h"

int client_main(int argc, char** argv) {

  if (argc != 2) {
    std::cout << "Usage: echo_client [MESSAGE]" << std::endl;
    return EXIT_FAILURE;
  }

  nanolibs::Socket client(nanolibs::socket::AddressFamily::Inet,
                          nanolibs::socket::SocketType::Stream,
                          nanolibs::socket::NetworkProtocol::Tcp,
                          nanolibs::socket::SocketBehavior::Block);

  client.connect("127.0.0.1", nanolibs::socket::AddressFamily::Inet, 9191);
  std::string msg(argv[1]);
  common::StreamHelper::sendString(client, msg);
  msg = common::StreamHelper::readString(client);
  std::cout << "Server: " << msg << std::endl;

  return EXIT_SUCCESS;

}

int main(int argc, char** argv) {
  try {
    return client_main(argc, argv);
  } catch (const nanolibs::socket::exception::Exception& ex) {
    std::cout << "----- Socket Exception -----" << std::endl;
    std::cout << "What: " << ex.what() << std::endl;
    std::cout << "Details: " << ex.details() << std::endl;
    std::cout << std::string(10, '=') << std::endl;
  }
}
