#pragma once

/**
 * @file    This file contains the definition of the actual os abstraction
 *          layer. It provides a direct but modern c++ representation of the
 *          POSIX socket api.
 */

// POSIX
#include <sys/socket.h>
#include <string_view>
#include <netinet/in.h>
#include <vector>

#include "nanolibs/socket/AddressFamily.h"
#include "nanolibs/socket/SocketType.h"
#include "nanolibs/socket/SocketBehavior.h"
#include "nanolibs/socket/NetworkProtocol.h"

#include "nanolibs/socket/Api.h"


//--- API
namespace nanolibs::socket::detail {

/**
 * Implements the Api interface in order to provide the POSIX conform socket
 * api.
 *
 * @details     See documentation of the Api interface to read more about the
 *              decision to abstract this class behind an interface.
 *
 *              This class is not meant to be used by the user of this library.
 *              The Socket class accepts an instance of any class implementing
 *              the Api interface and, with that, allows to mock the underlying
 *              socket api easily for testing purposes.
 *
 *              This class provides no additional features over the POSIX
 *              conform socket api but acts as an adapter (in terms of code
 *              style, error handling and data types) between the C and modern
 *              C++ code.
 *
 *              The datatype of the file descriptor is abstracted in the type
 *              definition 'FileDescriptor' but can be used (after being cast
 *              statically to int) anywhere else without any modifications.
 */
class POSIXSocket : public Api {
  //-- Members
public:
  //-- Constructor
  POSIXSocket() = default;
  ~POSIXSocket() = default;

  //-- Api
  /**
   * @brief     @see Api::create
   *
   * @details   Calls the POSIX conform 'socket' from the os socket api. All error codes are translated into c++
   *            exceptions. @see http://man7.org/linux/man-pages/man2/socket.2.html
   *
   * @param addressFamily Specifies the communication domain the socket is operating in. This will select the protocol
   *                      family which will be used for communication.
   *
   * @param type          Specifies the communication sematics.
   *
   * @param protocol      If the address family has more than one compatible protocol family it is necessary to be set.
   *                      Otherwise NetworkProtocol::Default can be used. (Passes 0 to socket).
   *
   * @param behavior      Specifies the behavior of the socket api or of the underlying file descriptor handling.
   *
   *
   * @throws exception::PermissionDenied                    Permission to create a socket of the specified type and/or
   *                                                        protocol is denied.
   *
   * @throws exception::AddressFamilyNotSupported           The implementation does not support the specified address
   *                                                        family.
   *
   * @throws exception::WrongProtocolOrSocketType           Unknown protocol, or protocol family not available or
   *                                                        invalid flags in socket type.
   *
   * @throws exception::MaximumOpenFileDescriptorsExceeded  The per-process or system-wide limit on the number of open
   *                                                        file descriptors has been reached.
   *
   *
   * @throws exception::InsufficientMemory                  Insufficient memory is available. The socket cannot be
   *                                                        created until sufficient resources are freed.
   *
   * @throws exception::ProtocolTypeNotSupported            The protocol type or the specified protocol is not supported
   *                                                        within this domain.
   *
   * @throws exception::UnknownError                        Call to socket returned an unknown error code
   *
   * @return    Returns the file descriptor returned by the socket function. If the operation fails an excetption is
   *            thrown.
   */
  FileDescriptor create(AddressFamily addressFamily, SocketType type, NetworkProtocol protocol,
                        SocketBehavior behavior) override;

  /**
   * @brief     @see Api::bind
   *
   * @details   Calls the POSIX conform 'bind' from the os socket api. All error codes are translated into c++
   *            exceptions. @see http://man7.org/linux/man-pages/man2/bind.2.html
   *
   * @param fd              The file descriptor refering to a socket.
   *
   * @param address         The address/name to assign to the socket.
   *
   * @param addressFamily   The family the address resides in.
   *
   * @param port            The port on the local endpoint that needs to be assigned to the socket. If the address
   *                        family doesn't support sockets leave the default parameter or set it to 0.
   *
   * @throws exception::PermissionDenied                        The address is protected, and the user is not the
   *                                                            superuser. For UNIX domain sockets also: Search
   *                                                            permission is denied on a component of the path prefix.
   *
   * @throws exception::AddressInUse                            The given address is already in use. Or for Internet
   *                                                            domain sockets: The port number was specified as zero in
   *                                                            the socket address structure, but, upon attempting to
   *                                                            bind to an ephemeral port, it was determined that all
   *                                                            port numbers in the ephemeral port range are currently
   *                                                            in use. See the discussion of
   *                                                            /proc/sys/net/ipv4/ip_local_port_range.
   *
   * @throws exception::BadFileDescriptor                       fd is not a valid file descriptor.
   *
   * @throws exception::SocketAlreadyBoundOrInvalidAddress      The socket is already bound to an address or addrlen is
   *                                                            wrong, or addr is not a valid address for this socket's
   *                                                            domain.
   *
   * @throws exception::FileDescriptorNotASocket                The file descriptor sockfd does not refer to a socket.
   *
   * @throws exception::NonLocalAddressOrInterfaceNotExisting   UNIX domain sockets: A nonexistent interface was
   *                                                            requested or the requested address was not local.
   *
   * @throws exception::ShouldNotHappenException                An error this library should have been prevented has
   *                                                            occurred. Please report this and the following error
   *                                                            code to the library maintainer.
   *
   * @throws exception::TooManySymbolicLinks                    UNIX domain sockets: Too many symbolic links were
   *                                                            encountered in resolving addr.
   *
   * @throws exception::AddressToLong                           UNIX domain sockets: addr is too long.
   *
   * @throws exception::MissingComponent                        UNIX domain sockets: A component in the directory prefix
   *                                                            of the socket pathname does not exist.
   *
   * @throws exception::InsufficientMemory                      UNIX domain sockets: Insufficient kernel memory was
   *                                                            available.
   *
   * @throws exception::ComponentIsNotADirectory                UNIX domain sockets: A component of the path prefix is
   *                                                            not a directory.
   *
   * @throws exception::ReadOnlyFilesystem                      UNIX domain sockets: The socket inode would reside on a
   *                                                            read-only filesystem.
   *
   * @throws exception::UnknownError                            Call to ::bind returned an unknown error code.
   */
  void bind(FileDescriptor fd, const std::string_view &address, AddressFamily addressFamily, int port) override;

  /**
   * @brief     @see Api::connect
   *
   * @details   Calls the POSIX conform 'connect' from the os socket api. All error codes are translated into c++
   *            exceptions. @see http://man7.org/linux/man-pages/man2/connect.2.html
   *
   * @param fd              The file descriptor refering to a socket.
   *
   * @param address         The address/name of the target endpoint which has a listening socket assigned to it.
   *
   * @param addressFamily   The family the address resides in.
   *
   * @param port            The port on the target endpoint. If the address family doesn't support sockets leave the
   *                        default parameter or set it to 0.
   *
   * @throws exception::PermissionDenied                      For UNIX domain sockets, which are identified by
   *                                                            pathname: Write permission is denied on the socket file,
   *                                                            or search permission is denied for one of the
   *                                                            directories in the path prefix.
   *                                                            Or:
   *                                                            The user tried to connect to a broadcast address without
   *                                                            having the socket broadcast flag enabled or the
   *                                                            connection request failed because of a local firewall
   *                                                            rule.
   *
   * @throws exception::AddressInUse                            Local address is already in use.
   *
   * @throws exception::NonLocalAddressOrInterfaceNotExisting  Internet domain sockets) The socket referred to by
   *                                                            sockfd had not previously been bound to an address and,
   *                                                            upon attempting to bind it to an ephemeral port, it was
   *                                                            determined that all port numbers in the ephemeral port
   *                                                            range are currently in use.
   *
   * @throws exception::AddressFamilyNotSupported              The passed address didn't have the correct address
   *                                                            family in its sa_family field.
   *
   * @throws exception::Again                                   For nonblocking UNIX domain sockets, the socket is
   *                                                            nonblocking, and the connection cannot be completed
   *                                                            immediately.  For other socket families, there are
   *                                                            insufficient entries in the routing cache.
   *
   * @throws exception::SocketAlreadyInUse                      The socket is nonblocking and a previous connection
   *                                                            attempt has not yet been completed.
   *
   * @throws exception::BadFileDescriptor                       fd is not a valid open file descriptor.
   *
   * @throws exception::ConnectionRefused                       A connect on a stream socket found no one listening on
   *                                                            the remote address.
   *
   * @throws exception::ShouldNotHappenException                An error this library should have been prevented has
   *                                                            occurred. Please report this and the following error
   *                                                            code to the library maintainer.
   *
   * @throws exception::ConnectionInProgress                  The socket is nonblocking and the connection cannot be
   *                                                            completed immediately. UNIX domain sockets failed with
   *                                                            EAGAIN instead.  It is possible to select(2) or poll(2)
   *                                                            for completion by selecting the socket for writing.
   *
   *                                                            After select(2) indicates writability, use getsockopt)2)
   *                                                            to read the SO_ERROR option at level SOL_SOCKET to
   *                                                            determine whether connect completed successfully
   *                                                            SO_ERROR is zero or unsuccessfully SO_ERROR is one of
   *                                                            the usual error codes listed here, explaining the reason
   *                                                            for the failure.
   *
   * @throws exception::Interrupted                             The system call was interrupted by a signal that was
   *                                                            caught.
   *
   * @throws exception::SocketAlreadyConnected                  The socket is already connected.
   *
   * @throws exception::NetworkUnreachable                      Network is unreachable.
   *
   * @throws exception::FileDescriptorNotASocket                The file descriptor sockfd does not refer to a socket.
   *
   * @throws exception::ProtocolTypeNotSupported              The socket type does not support the requested
   *                                                            communications protocol. This error can occur, for
   *                                                            example, on an attempt to connect a UNIX domain datagram
   *                                                            socket to a stream socket.
   *
   * @throws exception::TimedOut                                Timeout while attempting connection. The server may be
   *                                                            too busy to accept new connections. Note that for IP
   *                                                            sockets the timeout may be very long when syncookies are
   *                                                            enabled on the server.
   *
   * @throws exception::UnknownError                            connect return an unknown error code.
   */
  void connect(const FileDescriptor &fd, const std::string_view &address, AddressFamily addressFamily,
               int port) override;

  /**
   * @brief @see Api::listen
   *
   * @details   Calls the POSIX conform 'listen' from the os socket api. All error codes are translated into c++
   *            exceptions. @see http://man7.org/linux/man-pages/man2/connect.2.html
   *
   * @param fd      The file descriptor referring to a socket.
   *
   * @param backlog Specifies the maximum amount of pending connections handles. If this limit is being exhausted and a
   *                new connection request arrives, the connection may be refused.
   *
   * @throws exception::AddressInUse                Another socket is already listening on the same port.
   *
   *                                                Or (Internet domain sockets)
   *
   *                                                The socket referred to by sockfd had not previously been bound to an
   *                                                address and, upon attempting to bind it to an ephemeral port, it was
   *                                                determined that all port numbers in the ephemeral port range are
   *                                                currently in use. See the discussion of
   *                                                /proc/sys/net/ipv4/ip_local_port_range in ip(7).
   *
   * @throws exception::BadFileDescriptor           The argument sockfd is not a valid file descriptor.
   *
   * @throws exception::FileDescriptorNotASocket    The file descriptor sockfd does not refer to a socket.
   *
   * @throws exception::NotSupported                The socket is not of a type that supports the listen() operation.
   *
   * @throws exception::UnknownError                listen returned an unknown error code.
   */
  void listen(const FileDescriptor& fd, int backlog) override;

  /**
   * @brief     @see Api::accept
   *
   * @details   Calls the POSIX conform 'accept' from the os socket api. All error codes are translated into c++
   *            exceptions. @see http://man7.org/linux/man-pages/man2/accept.2.html
   *
   * @param fd The file descriptor referring to a socket.
   *
   * @throws exception::Again                  The socket is marked nonblocking and no connections are
   *                                                        present to be accepted. POSIX.1-2001 and POSIX.1-2008 allow
   *                                                        either error to be returned for this case, and do not
   *                                                        require these constants to have the same value, so a
   *                                                        portable application should check for both possibilities.
   *
   * @throws exception::BadFileDescriptor          Bad file descriptor
   *
   * @throws exception::ShouldNotHappenException      An error this library should have been prevented has
   *                                                            occurred. Please report this and the following error
   *                                                            code to the library maintainer.
   *
   * @throws exception::Interrupted              The system call was interrupted by a signal that was caught
   *                                                        before a valid connection arrived.
   *
   * @throws exception::SocketIsNotListening        Socket is not listening for connections.
   *
   * @throws exception::MaximumOpenFileDescriptorsExceeded  The per-process limit on the number of open file descriptors
   *                                                        has been reached.
   *
   * @throws exception::MaximumOpenFileDescriptorsExceeded  The system-wide limit on the total number of open files has
   *                                                        been reached.
   *
   * @throws exception::InsufficientMemory          Not enough free memory. This often means that the memory
   *                                                        allocation is limited by the socket buffer limits, not by
   *                                                        the system memory.
   *
   * @return Returns file descriptor referring to the socket of the accepted client connection.
   */
  FileDescriptor accept(const FileDescriptor &fd) override;

  /**
   * @brief     @see Api::accept
   *
   * @details   Calls the POSIX conform 'accept' from the os socket api. All error codes are translated into c++
   *            exceptions. @see http://man7.org/linux/man-pages/man2/accept.2.html
   *
   * @param fd              The file descriptor referring to a socket.
   *
   * @param address         An output value containing the remote address.
   *
   * @param addressFamily   The address family of the remote address.
   *
   * @param port            The port the connection has been accepted on.
   *
   * @param flags           Specifies the behavior of accept.
   *
   * @throws exception::Again                               The socket is marked nonblocking and no connections are
   *                                                        present to be accepted. POSIX.1-2001 and POSIX.1-2008 allow
   *                                                        either error to be returned for this case, and do not
   *                                                        require these constants to have the same value, so a
   *                                                        portable application should check for both possibilities.
   *
   * @throws exception::BadFileDescriptor                   Bad file descriptor
   *
   * @throws exception::ShouldNotHappenException            An error this library should have been prevented has
   *                                                        occurred. Please report this and the following error code to
   *                                                        the library maintainer.
   *
   * @throws exception::Interrupted                         The system call was interrupted by a signal that was caught
   *                                                        before a valid connection arrived.
   *
   * @throws exception::SocketIsNotListening                Socket is not listening for connections.
   *
   * @throws exception::MaximumOpenFileDescriptorsExceeded  The per-process limit on the number of open file descriptors
   *                                                        has been reached.
   *
   * @throws exception::MaximumOpenFileDescriptorsExceeded  The system-wide limit on the total number of open files has
   *                                                        been reached.
   *
   * @throws exception::InsufficientMemory                  Not enough free memory. This often means that the memory
   *                                                        allocation is limited by the socket buffer limits, not by
   *                                                        the system memory.
   *
   * @return Returns file descriptor referring to the socket of the accepted client connection.
   */
  FileDescriptor accept(const FileDescriptor &fd, const std::string_view &address, AddressFamily addressFamily,
                        int port, AcceptFlags flags) override;

  /**
   * @brief     @see Api::close
   *
   * @details   Calls the POSIX conform 'close' from the os socket api. All error codes are translated into c++
   *            exceptions. @see http://man7.org/linux/man-pages/man2/accept.2.html
   *
   * @param fd  The file descriptor referring to a socket.
   *
   * @throws exception::BadFileDescriptor   The argument is not a valid file descriptor.
   *
   * @throws exception::Interrupted         The close() function was interrupted by a signal.
   *
   * @throws exception::LowLevelIOError     An I/O error occurred while reading from or writing to the file system.
   *
   * @throws exception::UnknownError        close returned an unknown error code.
   */
  void close(const FileDescriptor &fd) override;

  /**
   * @brief     @see Api::write
   *
   * @details   Calls the POSIX conform 'write' from the os socket api. All error codes are translated into c++
   *            exceptions. @see http://man7.org/linux/man-pages/man2/accept.2.html
   *
   * @param fd      The file descriptor referring to a socket.
   *
   * @param data    The data to write to the socket.
   *
   * @throws exception::WouldBlock                  The file descriptor fd refers to a file other than a socket and has
   *                                                been marked non-blocking (O_NONBLOCK), and the write would block.
   *
   * @throws exception::BadFileDescriptor           fd is not a valid file descriptor or is not open for writing.
   *
   * @throws exception::DestinationAddressRequired  fd refers to a datagram socket for which a peer address has not been
   *                                                set using connect(2).
   *
   * @throws exception::DiskQuotaExhausted          The user's quota of disk blocks on the file system containing the
   *                                                file referred to by fd has been exhausted.
   *
   * @throws exception::ShouldNotHappenException    An error this library should have been prevented has occurred.
   *                                                Please report this and the following error code to the library
   *                                                maintainer.
   *
   * @throws exception::FileTooBig                  An attempt was made to write a file that exceeds the
   *                                                implementation-defined maximum file size or the process's file size
   *                                                limit, or to write at a position past the maximum allowed offset.
   *
   * @throws exception::Interrupted                 The call was interrupted by a signal before any data was written.
   *
   * @throws exception::TargetUnsuitableForWriting  fd is attached to an object which is unsuitable for writing; or the
   *                                                file was opened with the O_DIRECT flag, and either the address
   *                                                specified in buf, the value specified in count, or the current file
   *                                                offset is not suitably aligned.
   *
   * @throws exception::LowLevelIOError             A low-level I/O error occurred while modifying the inode.
   *
   * @throws exception::NotEnoughSpace              The device containing the file referred to by fd has no room for the
   *                                                data.
   *
   * @throws exception::PipeClosed                  fd is connected to a pipe or socket whose reading end is closed.
   *                                                When this happens the writing process will also receive a SIGPIPE
   *                                                signal. (Thus, the write return value is seen only if the program
   *                                                catches, blocks or ignores this signal.)
   *
   * @throws exception::UnknownError                write returned an unknown error code.
   */
  void write(const FileDescriptor &fd, const std::vector<uint8_t> &data) override;

  /**
   *
   * @param fd
   * @param data
   * @param count
   *
   * @throws exception::WouldBlock                  The file descriptor fd refers to a file other than a socket and has
   *                                                been marked non-blocking (O_NONBLOCK), and the read would block.
   *
   * @throws exception::BadFileDescriptor           fd is not a valid file descriptor or is not open for reading.
   *
   * @throws exception::ShouldNotHappenException    An error this library should have been prevented has occurred.
   *                                                Please report this and the following error code to the library
   *                                                maintainer.
   *
   * @throws exception::Interrupted                 The call was interrupted by a signal before any data was written.
   *
   * @throws exception::TargetUnsuitableForWriting  fd is attached to an object which is unsuitable for reading; or the
   *                                                file was opened with the O_DIRECT flag, and either the address
   *                                                specified in buf, the value specified in count, or the current file
   *                                                offset is not suitably aligned.\n - or -\nfd was created via a call
   *                                                to timerfd_create(2) and the wrong size buffer was given to read();
   *                                                see timerfd_create(2) for further information.
   *
   * @throws exception::LowLevelIOError             I/O error. This will happen for example when the process is in a
   *                                                background process group, tries to read from its controlling
   *                                                terminal, and either it is ignoring or blocking SIGTTIN or its
   *                                                process group is orphaned. It may also occur when there is a
   *                                                low-level I/O error while reading from a disk or tape.
   *
   * @throws exception::IsDirectory                 fd refers to a directory.
   *
   * @throws exception::UnknownError                read returned an unknown error code.
   *
   * @return    Returns the amount of bytes read from the socket.
   */
  size_t read(const FileDescriptor &fd, std::vector<uint8_t> &data, size_t count) override;

  /**
   *
   * @param fd
   * @param data
   * @param count
   * @param flags
   * @return
   */
  ssize_t recv(const FileDescriptor& fd, std::vector<uint8_t> &data, size_t count, ReceiveFlags flags) override;

  /**
   *
   * @param fd
   */
  void shutdown(const FileDescriptor& fd) override;
};

} // nanolibs::socket::detail::socket
