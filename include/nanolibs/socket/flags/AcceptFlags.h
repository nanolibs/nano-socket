#pragma once

#include <sys/types.h>
#include <sys/socket.h>

namespace nanolibs::socket {

enum class AcceptFlags {
  Default = 0,
  NonBlock = SOCK_NONBLOCK,
  CloseOnExec = SOCK_CLOEXEC
};

} // namespace nanolibs::socket
