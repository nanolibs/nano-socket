#pragma once

#include <sys/types.h>
#include <sys/socket.h>

namespace nanolibs::socket {

enum class ReceiveFlags {
  Default = 0,
  CloseOnExec = MSG_CMSG_CLOEXEC,
  DontWait = MSG_DONTWAIT,
  ErrorQueue = MSG_ERRQUEUE,
  OutOfBand = MSG_OOB,
  Peek = MSG_PEEK,
  Truncate = MSG_TRUNC,
  WaitAll = MSG_WAITALL
};

} // namespace nanolibs::socket
