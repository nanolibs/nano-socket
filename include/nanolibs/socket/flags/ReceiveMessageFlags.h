#pragma once

#include <sys/types.h>
#include <sys/socket.h>

namespace nanolibs::socket {

enum class ReceiveMessageFlags {
  Unset = 0,
  EndOfRecord = MSG_EOR,
  ReceivedError = MSG_ERRQUEUE,
  ReceivedOutOfBand = MSG_OOB,
  DataTruncated = MSG_TRUNC,
  ControlDataTruncated = MSG_TRUNC
};

} // namespace nanolibs::socket
