#pragma once
#include <netinet/in.h>

namespace nanolibs::socket {

/**
 * Domains of a socket instance in which the address has to be specified.
 */
enum class AddressFamily {
  Unix = AF_UNIX,
  Local = AF_LOCAL,
  Inet = AF_INET,
  AX25 = AF_AX25,
  IPX = AF_IPX,
  AppleTalk = AF_APPLETALK,
  X25 = AF_X25,
  Inet6 = AF_INET6,
  Decnet = AF_DECnet,
  Key = AF_KEY,
  NetLink = AF_NETLINK,
  Packet = AF_PACKET,
  RDS = AF_RDS,
  PPPOX = AF_PPPOX,
  LLC = AF_LLC,
  IB = AF_IB,
  MPLS = AF_MPLS,
  CAN = AF_CAN,
  TIPC = AF_TIPC,
  Bluetooth = AF_BLUETOOTH,
  Alg = AF_ALG,
  VSock = AF_VSOCK,
  KCM = AF_KCM
};

} // namespace nanolibs::socket
