#pragma once

#include <netinet/in.h>

namespace nanolibs::socket {

/**
 * Specifies how the socket or socket api behaves.
 */
enum class SocketBehavior {
  Block = 0,
  NonBlock = SOCK_NONBLOCK,
  CloseOnExec = SOCK_CLOEXEC
};

} // namespace nanolibs::socket




