#pragma once

#include <netinet/in.h>

namespace nanolibs::socket {

/**
 * Specifies how data is transferred.
 */
enum class NetworkProtocol {
  Ip = IPPROTO_IP,
  Hopopts = IPPROTO_HOPOPTS,
  Icmp = IPPROTO_ICMP,
  Igmp = IPPROTO_IGMP,
  Ipv4 = IPPROTO_IP,
  Tcp = IPPROTO_TCP,
  Egp = IPPROTO_EGP,
  Udp = IPPROTO_UDP,
  Dccp = IPPROTO_DCCP,
  Ipv6 = IPPROTO_IPV6,
  Routing = IPPROTO_ROUTING,
  Fragment = IPPROTO_FRAGMENT,
  Rsvp = IPPROTO_RSVP,
  Gre = IPPROTO_GRE,
  Esp = IPPROTO_ESP,
  Ah = IPPROTO_AH,
  Icmpv6 = IPPROTO_ICMPV6,
  None = IPPROTO_NONE,
  Dstopts = IPPROTO_DSTOPTS,
  Pim = IPPROTO_PIM,
  Sctp = IPPROTO_SCTP
};

} // namespace nanolibs::socket




