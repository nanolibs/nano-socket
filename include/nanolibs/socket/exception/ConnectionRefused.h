#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * A socket could not be connected to a remote endpoint.
 */
class ConnectionRefused : public Exception {
public:
  ConnectionRefused(const std::string_view& details) noexcept
      : Exception("Connection refused", details)
  { }
};

} // namespace nanolibs::socket::exception
