#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The file system is read only
 *
 * @detail
 */
class ReadOnlyFilesystem : public Exception {
public:
  ReadOnlyFilesystem(const std::string_view& details) noexcept
      : Exception("Read only filesystem", details)
  { }
};

} // namespace nanolibs::socket::exception
