#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The file descriptor is invalid.
 *
 * @detail
 */
class BadFileDescriptor : public Exception {
public:
  BadFileDescriptor(const std::string_view& details) noexcept
      : Exception("Bad file descriptor", details)
  { }
};

} // namespace nanolibs::socket::exception
