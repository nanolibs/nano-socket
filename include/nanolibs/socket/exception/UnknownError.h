#pragma once

#include "Exception.h"
#include <cstring>

namespace nanolibs::socket::exception {

/**
 * An unknown error occurred.
 */
class UnknownError : public Exception {
public:
  UnknownError(const std::string_view& details) noexcept : Exception("Unknown error", std::string(details) + ": " + std::strerror(errno)) { }
};

} // namespace nanolibs::socket::exception
