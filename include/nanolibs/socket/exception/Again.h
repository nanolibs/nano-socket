#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * A specific state of the socket requires the function to be called again.
 *
 * @detail
 */
  class Again : public Exception {
public:
  Again(const std::string_view& details) noexcept
      : Exception("Again", details)
  { }
};

} // namespace nanolibs::socket::exception
