#pragma once

#include <stdexcept>
#include <string_view>

namespace nanolibs::socket::exception {

/**
 * The address family specified for the socket is not supported.
 *
 * @detail
 */
class Exception : public std::runtime_error {

  /**
   * A descriptive explanation of the exception guiding the reader to the origin of the problem.
   */
  std::string m_details;

public:

  Exception(const std::string& what, const std::string_view& details) noexcept
    : std::runtime_error(what.c_str())
    , m_details(details)
  { }

  const std::string& details() const noexcept { return m_details; }
};

}

