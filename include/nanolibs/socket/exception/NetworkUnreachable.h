#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The network is unreachable
 *
 * @detail
 */
class NetworkUnreachable : public Exception {
public:
  NetworkUnreachable(const std::string_view& details) noexcept
      : Exception("Network unreachable", details)
  { }
};

} // namespace nanolibs::socket::exception
