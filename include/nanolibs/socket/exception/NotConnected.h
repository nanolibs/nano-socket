#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * An action that requires a connection was performed on an unconnected socket.
 *
 * @detail
 */
class NotConnected : public Exception {
public:
  NotConnected(const std::string_view& details) noexcept
      : Exception("Not connected", details)
  { }
};

} // namespace nanolibs::socket::exception
