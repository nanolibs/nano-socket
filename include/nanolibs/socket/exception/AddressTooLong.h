#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The address is too long
 *
 * @detail
 */
class AddressToLong : public Exception {
public:
  AddressToLong(const std::string_view& details) noexcept
      : Exception("Address too long", details)
  { }
};

} // namespace nanolibs::socket::exception
