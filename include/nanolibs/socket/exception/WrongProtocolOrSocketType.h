#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * Unknown protocol or socket type.
 *
 * @detail
 */
class WrongProtocolOrSocketType : public Exception {
public:
  WrongProtocolOrSocketType(const std::string_view& details) noexcept
      : Exception("Wrong protocol or socket type", details)
  { }
};

} // namespace nanolibs::socket::exception
