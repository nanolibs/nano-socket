#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The user's quota of disk blocks on the file system has been exhausted.
 *
 * @detail
 */
class DiskQuotaExhausted : public Exception {
public:
  DiskQuotaExhausted(const std::string_view& details) noexcept
      : Exception("Disk quota exhausted", details)
  { }
};

} // namespace nanolibs::socket::exception
