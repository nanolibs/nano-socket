#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * A connection attempt timed out
 *
 * @detail
 */
class TimedOut : public Exception {
public:
  TimedOut(const std::string_view& details) noexcept
      : Exception("Timed out", details)
  { }
};

} // namespace nanolibs::socket::exception
