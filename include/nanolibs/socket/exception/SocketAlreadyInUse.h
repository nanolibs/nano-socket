#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * An action has been performed twice on a socket while the first action has no finished.
 *
 * @detail
 */
class SocketAlreadyInUse : public Exception {
public:
  SocketAlreadyInUse(const std::string_view& details) noexcept
      : Exception("POSIXSocket already in use", details)
  { }
};

} // namespace nanolibs::socket::exception
