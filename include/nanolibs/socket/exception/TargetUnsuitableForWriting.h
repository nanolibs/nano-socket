#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * An object a file descriptor is referring to is not suitable for writing.
 *
 * @detail
 */
class TargetUnsuitableForWriting : public Exception {
public:
  TargetUnsuitableForWriting(const std::string_view& details) noexcept
      : Exception("Target unsuitable for writing", details)
  { }
};

} // namespace nanolibs::socket::exception
