#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * File descriptor refers to a directory.
 *
 * @detail
 */
class IsDirectory : public Exception {
public:
  IsDirectory(const std::string_view& details) noexcept
      : Exception("Is directory", details)
  { }
};

} // namespace nanolibs::socket::exception
