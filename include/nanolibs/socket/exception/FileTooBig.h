#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The file is too big.
 *
 * @detail
 */
class FileTooBig : public Exception {
public:
  FileTooBig(const std::string_view& details) noexcept
      : Exception("File too big", details)
  { }
};

} // namespace nanolibs::socket::exception
