#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * Accept has been called while socket is not listening.
 *
 * @detail
 */
class SocketIsNotListening : public Exception {
public:
  SocketIsNotListening(const std::string_view& details) noexcept
      : Exception("Socket is not listening", details)
  { }
};

} // namespace nanolibs::socket::exception
