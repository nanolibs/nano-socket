#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * A low level I/O error has occurred.
 *
 * @detail
 */
class LowLevelIOError : public Exception {
public:
  LowLevelIOError(const std::string_view& details) noexcept
      : Exception("Low level I/O error", details)
  { }
};

} // namespace nanolibs::socket::exception
