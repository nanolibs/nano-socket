#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The address is already in use.
 *
 * @detail
 */
class AddressInUse : public Exception {
public:
  AddressInUse(const std::string_view& details) noexcept
      : Exception("Address already in use", details)
  { }
};

} // namespace nanolibs::socket::exception
