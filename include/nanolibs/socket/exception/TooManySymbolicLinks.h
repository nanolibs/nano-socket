#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * A file ended up in too many symbolic link redirections.
 *
 * @detail
 */
class TooManySymbolicLinks : public Exception {
public:
  TooManySymbolicLinks(const std::string_view& details) noexcept
      : Exception("Too many symbolic links", details)
  { }
};

} // namespace nanolibs::socket::exception
