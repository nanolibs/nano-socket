#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The nano-socket library has a serious bug in the code base since it should've prevented this error.
 *
 * @detail
 */
class ShouldNotHappenException : public Exception {
public:
  ShouldNotHappenException(const std::string_view& details) noexcept
      : Exception("Should not happen!!!!!", details)
  { }
};

} // namespace nanolibs::socket::exception
