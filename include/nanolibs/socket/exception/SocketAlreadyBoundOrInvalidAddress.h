#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The socket has already been bound to an address.
 *
 * @detail
 */
class SocketAlreadyBoundOrInvalidAddress : public Exception {
public:
  SocketAlreadyBoundOrInvalidAddress(const std::string_view& details) noexcept
      : Exception("POSIXSocket already bound or invalid address", details)
  { }
};

} // namespace nanolibs::socket::exception
