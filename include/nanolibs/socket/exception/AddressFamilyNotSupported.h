#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The address family specified for the socket is not supported.
 *
 * @detail
 */
class AddressFamilyNotSupported : public Exception {
public:
  AddressFamilyNotSupported(const std::string_view& details) noexcept
    : Exception("Address family not supported", details)
    { }
};

} // namespace nanolibs::socket::exception
