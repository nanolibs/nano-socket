#pragma once


#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The system-wide or per-process limit of opened file descriptors has been exceeded.
 */
class MaximumOpenFileDescriptorsExceeded : public Exception {
public:
  MaximumOpenFileDescriptorsExceeded(const std::string_view& details) noexcept
      : Exception("Maximum open file descriptor exceeded", details)
  { }
};

} // namespace nanolibs::socket::exception
