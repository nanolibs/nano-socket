#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * A component is missing.
 *
 * @detail
 */
class MissingComponent : public Exception {
public:
  MissingComponent(const std::string_view& details) noexcept
      : Exception("Missing component", details)
  { }
};

} // namespace nanolibs::socket::exception
