#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * An operation failed due to insufficient memory.
 *
 * @detail
 */
class InsufficientMemory : public Exception {
public:
  InsufficientMemory(const std::string_view& details) noexcept
      : Exception("Insufficient memory", details)
  { }
};

} // namespace nanolibs::socket::exception
