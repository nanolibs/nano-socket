#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The other end of the pipe is closed.
 *
 * @detail
 */
class PipeClosed : public Exception {
public:
  PipeClosed(const std::string_view& details) noexcept
      : Exception("Pipe closed", details)
  { }
};

} // namespace nanolibs::socket::exception
