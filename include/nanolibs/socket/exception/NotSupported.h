#pragma once


#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * An unsupported action was performed on a socket.
 */
class NotSupported : public Exception {
public:
  NotSupported(const std::string_view& details) noexcept
      : Exception("Not Supported", details)
  { }
};

} // namespace nanolibs::socket::exception
