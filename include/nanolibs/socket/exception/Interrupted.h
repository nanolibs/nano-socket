#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * An signal interrupted a call.
 *
 * @detail
 */
class Interrupted : public Exception {
public:
  Interrupted(const std::string_view& details) noexcept
      : Exception("Interrupted", details)
  { }
};

} // namespace nanolibs::socket::exception
