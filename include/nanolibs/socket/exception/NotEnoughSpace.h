#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * Target has not enough space left to store data.
 *
 * @detail
 */
class NotEnoughSpace : public Exception {
public:
  NotEnoughSpace(const std::string_view& details) noexcept
      : Exception("Not enough space", details)
  { }
};

} // namespace nanolibs::socket::exception
