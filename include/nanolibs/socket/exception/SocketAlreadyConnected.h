#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * A connection has already been established.
 *
 * @detail
 */
class SocketAlreadyConnected : public Exception {
public:
  SocketAlreadyConnected(const std::string_view& details) noexcept
      : Exception("POSIXSocket already connected", details)
  { }
};

} // namespace nanolibs::socket::exception
