#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * A destination address is required.
 *
 * @detail
 */
class DestinationAddressRequired : public Exception {
public:
  DestinationAddressRequired(const std::string_view& details) noexcept
      : Exception("Destination address required", details)
  { }
};

} // namespace nanolibs::socket::exception
