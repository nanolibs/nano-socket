#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The component is not a directory
 *
 * @detail
 */
class ComponentIsNotADirectory : public Exception {
public:
  ComponentIsNotADirectory(const std::string_view& details) noexcept
      : Exception("Component is not a directory", details)
  { }
};

} // namespace nanolibs::socket::exception
