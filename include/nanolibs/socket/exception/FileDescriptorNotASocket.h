#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The file descriptor is not a socket
 *
 * @detail
 */
class FileDescriptorNotASocket : public Exception {
public:
  FileDescriptorNotASocket(const std::string_view& details) noexcept
      : Exception("File descriptor not a socket", details)
  { }
};

} // namespace nanolibs::socket::exception
