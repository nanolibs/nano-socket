#pragma once

#include "nanolibs/socket/exception/Exception.h"

namespace nanolibs::socket::exception {

/**
 * The operating system rejected the execution of an action due to insufficient permissions.
 *
 * @detail An action has been rejected due to insufficient permissions. Make sure the process is run as a user with
 * sufficient permissions and check if any security software is involved before choosing to run the process with
 * elevated privileges.
 */
class PermissionDenied : public Exception {
public:
  PermissionDenied(const std::string_view& details) noexcept
      : Exception("Permission denied", details)
  { }
};

} // namespace nanolibs::socket::exception
