#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * A connection attempt is still in progress
 *
 * @detail
 */
class ConnectionInProgress : public Exception {
public:
  ConnectionInProgress(const std::string_view& details) noexcept
      : Exception("Connection in progress", details)
  { }
};

} // namespace nanolibs::socket::exception
