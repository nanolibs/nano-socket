#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The address is not a local address or there is no interface for the given address.
 *
 * @detail
 */
class NonLocalAddressOrInterfaceNotExisting : public Exception {
public:
  NonLocalAddressOrInterfaceNotExisting(const std::string_view& details) noexcept
      : Exception("Non local address or interface not existing", details)
  { }
};

} // namespace nanolibs::socket::exception
