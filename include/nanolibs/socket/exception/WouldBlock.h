#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * An operation would block on a non-blocking socket.
 *
 * @detail
 */
  class WouldBlock : public Exception {
public:
  WouldBlock(const std::string_view& details) noexcept
      : Exception("Would block", details)
  { }
};

} // namespace nanolibs::socket::exception
