#pragma once

#include "Exception.h"

namespace nanolibs::socket::exception {

/**
 * The protocol is not supported for the address family and socket type combination.
 *
 * @detail
 */
class ProtocolTypeNotSupported : public Exception {
public:
  ProtocolTypeNotSupported(const std::string_view& details) noexcept
      : Exception("Protocol type not supported", details)
  { }
};

} // namespace nanolibs::socket::exception
