#pragma once

#include <netinet/in.h>

namespace nanolibs::socket {

/**
 * Specifies the kind of socket to use for IO.
 */
enum class SocketType {
  Stream = SOCK_STREAM,
  DGram = SOCK_DGRAM,
  SeqPacket = SOCK_SEQPACKET,
  Raw = SOCK_RAW,
  RDM = SOCK_RDM
};

} // namespace nanolibs::socket
