#pragma once

namespace nanolibs::socket {

/**
 * Wraps a value containing a system-wide identifier of a file.
 */
using FileDescriptor = int;

}
