#pragma once

/**
 * @file    This file contains the definition of the internal socket api.  It is used to encapsulate the OS api.
 */

// std
#include <string_view>
#include <vector>

// nanolibs::socket
#include "nanolibs/socket/AddressFamily.h"
#include "nanolibs/socket/Message.h"
#include "nanolibs/socket/NetworkProtocol.h"
#include "nanolibs/socket/SocketBehavior.h"
#include "nanolibs/socket/SocketType.h"
#include "nanolibs/socket/flags/AcceptFlags.h"
#include "nanolibs/socket/flags/ReceiveFlags.h"

namespace nanolibs::socket {

/**
 * Provides a basic interface of all basic, socket related functions used by this library.
 */
class Api {
public:
  Api() = default;
  virtual ~Api() = default;

/**
 * Creates a new socket and returns its file descriptor.
 */
  virtual FileDescriptor create(AddressFamily addressFamily, SocketType type,
                                NetworkProtocol networkProtocol, SocketBehavior behavior) = 0;

/**
 * Binds a socket to an address (or any) and to a port.
 *
 * @param fd The file descriptor of the socket.
 * @param address The address to bind the socket to (use address::Any to bind to all addresses)
 * @param addressFamily The type of address (e.g. 127.0.0.1 = Inet, :: = Inet6, and so on...)
 * @param port The port to bind to.
 */
  virtual void bind(FileDescriptor fd,
                    const std::string_view &address,
                    AddressFamily addressFamily,
                    int port) = 0;

/**
 * Connects an existing socket to a given address.
 *
 * @param fd The file descriptor of the socket.
 * @param address The address to connect the socket to (use address::Any to bind to all addresses)
 * @param addressFamily The type of address (e.g. 127.0.0.1 = Inet, :: = Inet6, and so on...)
 * @param port The port to connect to.
 */
  virtual void connect(const FileDescriptor &fd,
                       const std::string_view &address,
                       AddressFamily addressFamily,
                       int port) = 0;

  /**
   * Enable a socket to listen on the bound endpoint.
   *
   * @param fd The file descriptor of the socket.
   * @param backlog The amount of connections queued when busy before connection is refused.
   */
  virtual void listen(const FileDescriptor& fd, int backlog) = 0;

  /**
   * Accepts a new connection and returns the file descriptor of the new socket.
   *
   * @details This is a blocking function and only returns if a new connection has been accepted.
   *
   * @param fd The file descriptor of the socket.
   * @return A file descriptor of the socket connected to the accepted client.
   */
  virtual FileDescriptor accept(const FileDescriptor &fd) = 0;

  /**
   * Accepts a new connection on a specific endpoint and returns the file descriptor of the new socket.
   *
   * @param fd The file descriptor of the socket.
   * @param address
   * @param addressFamily
   * @param port
   * @param flags
   * @return A file descriptor of the socket connected to the accepted client.
   */
  virtual FileDescriptor accept(const FileDescriptor &fd,
                                const std::string_view &address,
                                AddressFamily addressFamily,
                                int port,
                                AcceptFlags flags) = 0;

  /**
 * Closes the socket.
 *
 * @param fd The file descriptor of the socket.
 */
  virtual void close(const FileDescriptor &fd) = 0;

/**
 * Writes to a socket.
 *
 * @param fd The file descriptor of the socket.
 * @param data The container containing the bytes to write.
 */
  virtual void write(const FileDescriptor &fd, const std::vector<uint8_t> &data) = 0;

/**
 * Reads from a socket.
 *
 * @param fd The file descriptor of the socket.
 * @param data The container to store the received bytes into.
 * @param count The amount of data to read.
 */
  virtual size_t read(const FileDescriptor &fd, std::vector<uint8_t> &data, size_t count) = 0;

  /**
   *
   * @param fd
   * @param data
   * @param count
   * @param flags
   * @return
   */
  virtual ssize_t recv(const FileDescriptor& fd, std::vector<uint8_t> &data, size_t count, ReceiveFlags flags) = 0;

  /**
   *
   * @param fd
   */
  virtual void shutdown(const FileDescriptor& fd) = 0;
};

} // nanolibs::socket
