#pragma once

#include "nanolibs/socket/types.h"
#include "nanolibs/socket/Api.h"

#include "nanolibs/socket/AddressFamily.h"
#include "nanolibs/socket/SocketBehavior.h"
#include "nanolibs/socket/SocketType.h"
#include "nanolibs/socket/NetworkProtocol.h"

#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>

#include "nanolibs/socket/exceptions.h"

/**
 * @file
 */

namespace nanolibs {

class Socket final {

  socket::FileDescriptor m_fd;
  std::shared_ptr<socket::Api> m_api;

public:
  Socket(const socket::FileDescriptor& fd) noexcept;
  Socket(const std::shared_ptr<socket::Api>& api, const socket::FileDescriptor& fd) noexcept;

  Socket(socket::AddressFamily addressFamily,
         socket::SocketType type,
         socket::NetworkProtocol networkProtocol,
         socket::SocketBehavior behavior) noexcept;

  Socket(const std::shared_ptr<socket::Api>& api,
         socket::AddressFamily addressFamily,
         socket::SocketType type,
         socket::NetworkProtocol networkProtocol,
         socket::SocketBehavior behavior) noexcept;
  ~Socket();

  void bind(const std::string_view &address,
            socket::AddressFamily addressFamily,
            int port);

  void connect(const std::string_view &address,
               socket::AddressFamily addressFamily,
               int port);

  void listen(int backlog);

  std::unique_ptr<Socket> accept();

  std::unique_ptr<Socket> accept(const std::string_view &address,
                        socket::AddressFamily addressFamily,
                        int port,
                        socket::AcceptFlags flags = socket::AcceptFlags::Default);


  void write(const std::vector<uint8_t> &data);

  size_t read(std::vector<uint8_t> &data, size_t count);

  socket::FileDescriptor fileDescriptor() const noexcept;

  ssize_t recv(std::vector<uint8_t> &data, size_t count, socket::ReceiveFlags flags = socket::ReceiveFlags::Default);

private:

  void close();
};

} // namespace nanolibs
